/////////////////////////////////////////////////////////////////////////////
////
///// University of Hawaii, College of Engineering
///// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
/////
///// @file catDatabase.h
///// @version 1.0
/////
///// @author Byron Soriano <byrongs@hawaii.edu>
///// @date 21_Feb_2022
///////////////////////////////////////////////////////////////////////////////
////


#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

#define MAX_CAT_NAME 30 // max characters in name
#define MAX_CAT 50 //max space available

size_t numCats;

char name;


enum gender {UNKNOWN_GENDER, MALE, FEMALE};


enum breed {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};


bool isFixed;


float weight;





