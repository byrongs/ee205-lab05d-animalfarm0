###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 
###
### 
###
### @Byron Sorianor <byrongs@hawaii.edu>
### @Feb 20 2022   
###
### 
###############################################################################
CC     = gcc
CFLAGS = -g -Wall 

TARGET = main


all = $(TARGET)



catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c

addCats.o: addCats.c catDatabase.h
	$(CC) $(CFLAGS) -c addCats.c

deleteCats.o: deleteCats.c deleteCats.h
	$(CC) $(CFLAGS) -c deleteCats.c

updateCats.o: updateCats.c updateCats.h
	$(CC) $(CFLAGS) -c updateCats.c

reportCats.o: reportCats.c reportCats.h
	$(CC) $(CFLAGS) -c  reportCats.c


main: main.o catDatabase.o addCats.o deleteCats.o reportCats.o updateCats.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o catDatabase.o addCats.o deleteCats.o reportCats.o updateCats.o

test: $(TARGET)
	./$(TARGET)
clean:
	rm -f $(TARGET) *.o
